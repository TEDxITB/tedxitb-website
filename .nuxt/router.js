import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _a7bdfdd4 = () => import('../pages/thank-you.vue' /* webpackChunkName: "pages/thank-you" */).then(m => m.default || m)
const _53b6fb01 = () => import('../pages/contact-us.vue' /* webpackChunkName: "pages/contact-us" */).then(m => m.default || m)
const _9aa34b32 = () => import('../pages/about.vue' /* webpackChunkName: "pages/about" */).then(m => m.default || m)
const _4889fd21 = () => import('../pages/coming-soon.vue' /* webpackChunkName: "pages/coming-soon" */).then(m => m.default || m)
const _40729b2c = () => import('../pages/gallery.vue' /* webpackChunkName: "pages/gallery" */).then(m => m.default || m)
const _403215ed = () => import('../pages/speakers/index.vue' /* webpackChunkName: "pages/speakers/index" */).then(m => m.default || m)
const _0336af95 = () => import('../pages/speakers/_id.vue' /* webpackChunkName: "pages/speakers/_id" */).then(m => m.default || m)
const _7d2ab494 = () => import('../pages/index_old.vue' /* webpackChunkName: "pages/index_old" */).then(m => m.default || m)
const _5b3395a8 = () => import('../pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/thank-you",
			component: _a7bdfdd4,
			name: "thank-you"
		},
		{
			path: "/contact-us",
			component: _53b6fb01,
			name: "contact-us"
		},
		{
			path: "/about",
			component: _9aa34b32,
			name: "about"
		},
		{
			path: "/coming-soon",
			component: _4889fd21,
			name: "coming-soon"
		},
		{
			path: "/gallery",
			component: _40729b2c,
			name: "gallery"
		},
		{
			path: "/speakers",
			component: _403215ed,
			name: "speakers"
		},
		{
			path: "/speakers/:id",
			component: _0336af95,
			name: "speakers-id"
		},
		{
			path: "/index:old",
			component: _7d2ab494,
			name: "indexold"
		},
		{
			path: "/",
			component: _5b3395a8,
			name: "index"
		}
    ],
    
    
    fallback: false
  })
}
