const path = require('path')
const PurgecssPlugin = require('purgecss-webpack-plugin')
const glob = require('glob-all')

class TailwindExtractor {
    static extract(content) {
        return content.match(/[A-z0-9-:/]+/g) || []
    }
}

//module.exports.meta = require('~/node_modules/vue-gallery/package.json')

module.exports = {
    /*
     ** Headers of the page
     */
    head: {
        title: 'TEDxITB 2018',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: 'Website TedXITB' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Fira+Sans'}
        ]
    },
    /*
     ** Customize the progress bar color
     */
    loading: { color: '#FF0000' },
    /*
     ** Build configuration
     */
    build: {
        /*
         ** Run ESLint on save
         */
        extractCSS: true,
        postcss: [
            require('tailwindcss')('./tailwind.js'),
            require('autoprefixer')
        ],
        extend(config, { isDev }) {
            if (!isDev) {
                config.plugins.push(
                    new PurgecssPlugin({
                        // purgecss configuration
                        // https://github.com/FullHuman/purgecss
                        paths: glob.sync([
                            path.join(__dirname, './pages/**/*.vue'),
                            path.join(__dirname, './layouts/**/*.vue')
                        ]),
                        extractors: [{
                            extractor: TailwindExtractor,
                            extensions: ['vue']
                        }],
                        whitelist: ['html', 'body']
                    })
                )
            }
        }
    },
    css: ['~/assets/css/tailwind.css', '~/assets/css/transition.css']
}